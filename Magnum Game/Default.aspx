﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Magnum_Game.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>The MAGNUM</title>
    <asp:Literal runat="server" ID="GotPrize"></asp:Literal>
    <link href="Content/Site.css" rel="stylesheet" />
    <script src="Scripts/preloadjs-NEXT.min.js"></script>
    <script src="Scripts/SoundJS.js"></script>
    <script src="Scripts/HTMLAudioPlugin.js"></script>
    <script src="Scripts/FlashPlugin.js"></script>
    <script src="Scripts/swfobject.js"></script>
    <script src="Scripts/jquery-1.8.3.min.js"></script>
    <script src="Scripts/Site.js?time=123456"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="fontMeasurer"></div>
        <div class="container" id="splash">
            <a class="start-button">NHẤN ENTER ĐỂ "SĂN" MAGNUM</a>
        </div>
        <div class="container" id="textContainer">
        </div>
        <div class="container" id="resultPane">
            <canvas id="resultCanvas"></canvas>
            <canvas id="flowerCanvas"></canvas>
            <table>
                <tr>
                    <td>
                        <span id="resultText"></span>
                        <br />
                        <a class="end-button">NHẤN ENTER ĐỂ BẮT ĐẦU LẠI</a>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
