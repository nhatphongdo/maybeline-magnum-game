﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Step1.aspx.cs" Inherits="Magnum_Game.MakeupStep1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {
            initFileUploads();
            $('#FileUpload').live('change', function () {
                uploadFile();
            });

            //if ($('#backgroundtransparent').css("display") == "block") {
            //    $('.upload_image').hide();
            //}
            //else {
            //    $('.upload_image').show();
            //}
        });
        function start() {
            $(".game_content").fadeIn();
            $("#backgroundtransparent").hide();
            $("#start_button").hide();
            $('.upload_image').fadeIn();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<div class="game_content" style="display:none;">
        <div class="images_use">
            <div style="color: #e5c746; text-transform: uppercase; margin-bottom: 20px;">
                hình ảnh khuyến khích dùng
            </div>
            <img src="Images/use_1.jpg" alt="" />
            <img src="Images/use_2.jpg" alt="" />
            <div class="hspace"></div>
            <ul>
                <li>Hình ảnh trực diện</li>
                <li>Ánh sáng hài hòa</li>
                <li>Không trang điểm, tóc không che mặt</li>
                <li>Dung lượng hình < 2MB</li>
            </ul>
        </div>
        <div class="images_notuse">
            <div style="color: #e5c746; text-transform: uppercase; margin-bottom: 20px;">
                hình ảnh không nên dùng
            </div>
            <img src="Images/not_use.jpg" alt="" />
            <div class="hspace"></div>
            <ul>
                <li>Góc mặt khuất</li>
                <li>Thiếu sáng</li>
                <li>Chất lượng hình không đảm bảo</li>
            </ul>
        </div>
      
        <div class="clear"></div>
    </div>--%>
    <asp:FileUpload runat="server" ID="FileUpload" ClientIDMode="Static" />
    <div>
    </div>
    <div class="error">
        <asp:Literal runat="server" ID="ErrorMessage"></asp:Literal>
    </div>
    <div style="width: 100%; height: 100%; background-color: black; opacity: 0.6; display: block; z-index: 1000; position: absolute; top: 0; left: 0;" id="backgroundtransparent"></div>
    <a class="button_upload" id="start_button" style="position: absolute; top: 300px; left: 435px; z-index: 1200" onclick="start()">START</a>
</asp:Content>
