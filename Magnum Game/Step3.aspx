﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Step3.aspx.cs" Inherits="Magnum_Game.Step3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/Makeup.js"></script>
    <script type="text/javascript">
        function tabstrans(id) {
            id = id.toString();
            $('#face').removeClass('selected_pro');
            $('#lip').removeClass('selected_pro');
            $('#eye').removeClass('selected_pro');
            $(".choose_product_face").hide();
            $(".choose_product_lip").hide();
            $(".choose_product_eye").hide();

            $('#' + id).addClass('selected_pro');
            if (id == 'face') {
                $(".choose_product_face").fadeIn();
                $(".choose_product_lip").fadeOut();
                $(".choose_product_eye").fadeOut();
            }
            else if (id == 'lip') {
                $(".choose_product_face").fadeOut();
                $(".choose_product_lip").fadeIn();
                $(".choose_product_eye").fadeOut();
            }
            else if (id == 'eye') {
                $(".choose_product_face").fadeOut();
                $(".choose_product_lip").fadeOut();
                $(".choose_product_eye").fadeIn();
            }
        }

        function SelectFace() {
            var tabselected = $(".selected_pro").attr("id");
            var tabclass = "";
            if (tabselected == "face") {
                tabclass = "choose_product_face";
                var note1 = $("." + tabclass).children(".note1");
                var note2 = $("." + tabclass).children(".note2");
                var note3 = $("." + tabclass).children(".note3");

                if (note1.css("display") == "block" && note2.css("display") == "none" && note3.css("display") == "none") {
                    note1.hide();
                    note2.fadeIn();
                }
                else if (note1.css("display") == "none" && note2.css("display") == "block" && note3.css("display") == "none") {
                    note2.hide();
                    note3.fadeIn();
                }
                else if (note1.css("display") == "none" && note2.css("display") == "none" && note3.css("display") == "block")
                {
                    $(".choose_product_face").hide();
                    $(".choose_product_lip").fadeIn();
                    $(".choose_product_eye").hide();
                    ClearClass();
                    $('#lip').addClass('selected_pro');
                }
            }
            else if (tabselected == "lip") {
                tabclass = "choose_product_lip";
                var note1 = $("." + tabclass).children(".note1");
                var note2 = $("." + tabclass).children(".note2");

                if (note1.css("display") == "block" && note2.css("display") == "none") {
                    note1.hide();
                    note2.fadeIn();
                }
                else if (note1.css("display") == "none" && note2.css("display") == "block")
                {
                    $(".choose_product_face").hide();
                    $(".choose_product_lip").hide();
                    $(".choose_product_eye").fadeIn();
                    ClearClass();
                    $('#eye').addClass('selected_pro');
                }
            }
            else if (tabselected == "eye") {
                tabclass = "choose_product_eye";
            }

        }

        function PrevFace() {
            var tabselected = $(".selected_pro").attr("id");
            var tabclass = "";
            if (tabselected == "face") {
                tabclass = "choose_product_face";
                var note1 = $("." + tabclass).children(".note1");
                var note2 = $("." + tabclass).children(".note2");
                var note3 = $("." + tabclass).children(".note3");

                if (note1.css("display") == "none" && note2.css("display") == "none" && note3.css("display") == "block") {
                    note3.hide();
                    note2.fadeIn();
                }
                else if (note1.css("display") == "none" && note2.css("display") == "block" && note3.css("display") == "none") {
                    note2.hide();
                    note1.fadeIn();
                }
            }
            else if (tabselected == "lip") {
                tabclass = "choose_product_lip";
                var note1 = $("." + tabclass).children(".note1");
                var note2 = $("." + tabclass).children(".note2");

                if (note1.css("display") == "none" && note2.css("display") == "block") {
                    note2.hide();
                    note1.fadeIn();
                }
                else if (note1.css("display") == "block" && note2.css("display") == "none")
                {
                    $(".choose_product_face").fadeIn();
                    $(".choose_product_lip").hide();
                    $(".choose_product_eye").hide();
                    ClearClass();
                    $('#face').addClass('selected_pro');
                }
            }
            else if (tabselected == "eye") {
                tabclass = "choose_product_eye";
                $(".choose_product_face").hide();
                $(".choose_product_lip").fadeIn();
                $(".choose_product_eye").hide();
                ClearClass();
                $('#lip').addClass('selected_pro');
            }

        }

        function ClearClass()
        {
            $('#face').removeClass('selected_pro');
            $('#lip').removeClass('selected_pro');
            $('#eye').removeClass('selected_pro');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="game_content" style="width: 800px;">
        <div class="img_step3">
            <asp:HiddenField ID="ImageUrl" runat="server" />
            <div id="imageEditor"></div>
            <div class="scrollbar">
                <asp:HiddenField ID="HiddenField1" runat="server" />
                <div style="margin-top: 10px;">
                    <input class="easyui-slider" value="1" style="width: 130px" data-options="
                            max:9,
                            min:1,
				            showTip: false,
				            tipFormatter: function(value){
					            return value;
				            },
				            onChange: function(value){
                                Zoom(value);
				            }" />
                </div>
                
            </div>
        </div>
        <div class="content_step3">
            <ul class="menu_step3">
                <li><a href="javascript:tabstrans('face');" id="face" class="selected_pro">MẶT</a></li>
                <li><a href="javascript:tabstrans('lip');" id="lip">MÔI</a></li>
                <li><a href="javascript:tabstrans('eye');" id="eye">MẮT</a></li>
            </ul>
            <div class="clear"></div>
            <div class="choose_product_face" style="display: block;">
                <div class="note1" style="display: block;">
                    <div class="title_play">
                        Các bước trang điểm để có 1 lớp nền hiệu quả
                    </div>
                    <img src="Images/face.png" style="float: left" />
                    <div class="content_desc">
                        <div class="desc_prod">
                            Bước 1 : BB Cream
                        </div>
                        <div class="desc_prod" style="width: auto; padding-top: 10px;">
                            Chọn màu                  
                        </div>
                        <a href="#">
                            <img src="Images/light.png" alt="" style="margin-left: 20px;" /></a>
                        <a href="#">
                            <img src="Images/skin.png" alt="" style="margin-left: 20px;" /></a>
                        <div class="desc_prod">
                            Đậm nhạt
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="note2" style="display: none;">
                    <div class="title_play">
                        Các bước trang điểm để có 1 lớp nền hiệu quả
                    </div>
                    <img src="Images/face_2.png" style="float: left" />
                    <div class="content_desc">
                    <div class="desc_prod">
                        Bước 2 : BB SILK
Phấn nền chống nhờn siêu mịn
                    </div>
                    <div class="desc_prod" style="width: auto; padding-top: 10px;">
                        Chọn màu                  
                    </div>
                    <a href="#">
                        <img src="Images/light.png" alt=""  style="margin-left: 10px;" /></a>
                    <a href="#">
                        <img src="Images/skin.png" alt="" style="margin-left: 10px;" /></a>
                    <a href="#">
                        <img src="Images/dark.png" alt="" style="margin-left: 10px;" /></a>
                    <div class="desc_prod">
                        Đậm nhạt
                    </div>
                        </div>
                    <div class="clear"></div>
                </div>
                <div class="note3" style="display: none;">
                    <div class="title_play">
                        Các bước trang điểm để có 1 lớp nền hiệu quả
                    </div>
                    <img src="Images/face_1.png" style="float: left" />
                    <div class="content_desc">
                        <div class="desc_prod">
                            Bước 3 : CLEAR SMOOTH BLUSH
                        </div>
                        <div class="desc_prod" style="width: auto; padding-top: 10px;">
                            Chọn màu                  
                        </div>
                        <a href="#">
                            <img src="Images/light.png" alt="" style="margin-left: 20px;" /></a>
                        <a href="#">
                            <img src="Images/skin.png" alt="" style="margin-left: 20px;" /></a>
                        <div class="desc_prod">
                            Đậm nhạt
                        </div>
                    <div style="margin-top: 2px; margin-left: 195px;">
                        <input class="easyui-slider" value="1" style="width: 130px" data-options="
                            max:9,
                            min:1,
				            showTip: false,
				            tipFormatter: function(value){
					            return value;
				            },
				            onChange: function(value){
					            
				            }" />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="choose_product_lip">
                <div class="note1" style="display: block;">
                    <div class="title_play">
                        Các bước trang điểm để có 1 lớp nền hiệu quả
                    </div>
                    <img src="Images/lip.png" style="float: left" />
                    <div class="content_desc">
                        <div class="desc_prod">
                            Bước 1 : BABY LIPS COLOR
                        </div>
                        <div class="desc_prod" style="width: auto; padding-top: 10px;">
                            Chọn màu                  
                        </div>
                        <a href="#">
                            <img src="Images/light.png" alt="" style="margin-left: 20px;" /></a>
                        <a href="#">
                            <img src="Images/skin.png" alt="" style="margin-left: 20px;" /></a>
                        <div class="desc_prod">
                            Đậm nhạt
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="note2" style="display: none;">
                    <div class="title_play">
                        Các bước trang điểm để có 1 lớp nền hiệu quả
                    </div>
                    <img src="Images/lip.png" style="float: left" />
                    <div class="content_desc">
                        <div class="desc_prod">
                            Bước 2 : COLOR SENSATIONAL MOISTURE EXTREME
                        </div>
                        <div class="desc_prod" style="width: auto; padding-top: 10px;">
                            Chọn màu                  
                        </div>
                        <a href="#">
                            <img src="Images/light.png" alt="" style="margin-left: 20px;" /></a>
                        <a href="#">
                            <img src="Images/skin.png" alt="" style="margin-left: 20px;" /></a>
                        <div class="desc_prod">
                            Đậm nhạt
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="choose_product_eye">
                c
            </div>
            <a href="#" class="button_upload" style="margin: 0; float: left;" onclick="PrevFace()">Quay lại</a>
            <a href="#" class="button_upload" style="margin: 0; float: left;" onclick="SelectFace()">Tiếp tục</a>
        </div>
        <div class="hspace" style="height: 30px;"></div>
    </div>
</asp:Content>
