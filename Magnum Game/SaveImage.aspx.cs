﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnum_Game
{
    public partial class SaveImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["image"]))
            {
                var base64Image = Request["image"];

                // Strip
                var index = base64Image.IndexOf(',');
                base64Image = base64Image.Substring(index + 1).TrimStart();
                base64Image = base64Image.Replace(' ', '+');

                // Convert Base64 String to byte[]
                var imageBytes = Convert.FromBase64String(base64Image);
                var ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                // Convert byte[] to Image
                ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);

                var path = string.Format("Result/{0}.jpg", DateTime.Now.Ticks);
                image.Save(Server.MapPath(path), System.Drawing.Imaging.ImageFormat.Jpeg);

                Response.Write(path);
            }
        }
    }
}