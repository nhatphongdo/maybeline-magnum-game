﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnum_Game
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var fileName = Request["place"];
            if (string.IsNullOrEmpty(fileName))
            {
                fileName = "GiftCount";
            }

            var text = "0";
            if (System.IO.File.Exists(Server.MapPath(fileName + ".txt")))
            {
                text = System.IO.File.ReadAllText(Server.MapPath(fileName + ".txt"));
            }

            var count = 0;
            if (!int.TryParse(text, out count))
            {
                count = 0;
            }

            if (count <= 0)
            {
                GotPrize.Text = "<script type='text/javascript'>gotPrize = false;</script>";
            }
            else
            {
                var random = new Random((int)DateTime.Now.Ticks);
                if (random.Next(100) % 9 == 0)
                {
                    GotPrize.Text = "<script type='text/javascript'>gotPrize = true;</script>";
                    --count;
                    System.IO.File.WriteAllText(Server.MapPath(fileName + ".txt"), count.ToString());
                }
                else
                {
                    GotPrize.Text = "<script type='text/javascript'>gotPrize = false;</script>";
                }
            }
        }
    }
}