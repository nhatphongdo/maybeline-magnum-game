﻿//Format FileUpload
var W3CDOM = (document.createElement && document.getElementsByTagName);

function initFileUploads() {
    if (!W3CDOM) return;
    var fakeFileUpload = document.createElement('div');
    var fileInput = document.createElement('input');
    fileInput.setAttribute("readonly", "readonly");
    fileInput.setAttribute("placeholder", "Tập tin ảnh");
    fileInput.className = 'textbox upload-input hidden';

    fakeFileUpload.appendChild(fileInput);
    var button = document.createElement('a');
    button.className = 'button_upload upload_image';
    button.setAttribute("style", "margin-top: 100px;margin-left:435px; display:none;");
    button.textContent = "Tải ảnh lên";
    fakeFileUpload.appendChild(button);
    var x = document.getElementsByTagName('input');
    for (var i = 0; i < x.length; i++) {
        if (x[i].type != 'file') continue;
        x[i].className = 'hidden';
        var clone = fakeFileUpload.cloneNode(true);
        x[i].parentNode.appendChild(clone);
        x[i].relatedElement = clone.getElementsByTagName('input')[0];
        x[i].relatedElement.onclick = function () {
            this.parentNode.parentNode.getElementsByTagName('input')[0].click();
        };
        clone.getElementsByTagName('a')[0].onclick = function () {
            this.parentNode.parentNode.getElementsByTagName('input')[0].click();
        };
        x[i].onchange = function () {
            this.relatedElement.value = this.value;
        };
        x[i].onselect = function () {
            this.relatedElement.select();
        };
    }
}
function uploadFile() {
    $("#form1").submit();
}


