var preload, soundHanlder;

var manifest = [
    { id: "BackgroundNew", src: "Content/backgroundnew.mp3" }
];

var stage, viewport, imageLayer, regionLayer, curveLayer, drawLayer, creamImage, silkImage, cheekImage1, cheekImage2, eyeImage1, eyeImage2, lip1Image, lip2Image, teethImage;

var regions = new Array();
var currentId = "", isReleased = false;

var scale = 1;

var zIndexes = ["face", "eyeleft", "eyeright", "lipnolaugh", "teeth", "cheekleft", "cheekright"];

var colors = new Array();

var foundationFade = 5, foundationType = '';
var silkFade = 5, silkType = '';

var cheekFade = 5, cheekType = 0, cheekColor = '';

var lip1Fade = 7, lip1Type = '';
var lip2Fade = 7, lip2Type = '';
var openMouth;
var eyeSrc1 = "", eyeSrc2 = "";


function startGame() {
    createjs.FlashPlugin.BASE_PATH = "Content/";
    createjs.SoundJS.registerPlugins([createjs.FlashPlugin]);
    preload = new createjs.PreloadJS();
    preload.installPlugin(createjs.SoundJS);
    preload.loadManifest(manifest);
    preload.onComplete = function () {
        if (window.location.href.toLowerCase().indexOf("step2.aspx") >= 0) {
            // Play sound
            soundHanlder = createjs.SoundJS.play("BackgroundNew", createjs.SoundJS.INTERRUPT_ANY, 0, 0, -1, 1, 0);
        }
    };

    var ImageUrl = $("#ImageUrl").val();

    stage = new Kinetic.Stage({
        container: "imageEditor",
        width: 350,
        height: 400
    });

    imageLayer = new Kinetic.Layer();
    regionLayer = new Kinetic.Layer();
    curveLayer = new Kinetic.Layer();
    drawLayer = new Kinetic.Layer();

    loadImage(ImageUrl);

    /*
    * add custom property curve objects to layer so that
    * they can be modified by reference
    */
    regions = new Array();

    regions["face"] = [
        {
            x: 260,
            y: 80
        },
        {
            x: 200,
            y: 50
        },
        {
            x: 140,
            y: 80
        },
        {
            x: 120,
            y: 140
        },
        {
            x: 140,
            y: 200
        },
        {
            x: 200,
            y: 240
        },
        {
            x: 260,
            y: 200
        },
        {
            x: 280,
            y: 140
        }
    ];
    regions["eyeleft"] = [
        {
            x: 170,
            y: 115
        },
        {
            x: 175,
            y: 110
        },
        {
            x: 160,
            y: 105
        },
        {
            x: 145,
            y: 110
        },
        {
            x: 150,
            y: 115
        },

        {
            x: 160,
            y: 118
        }

    ];
    regions["eyeright"] = [
    {
        x: 250,
        y: 115
    },
    {
        x: 255,
        y: 110
    },
    {
        x: 240,
        y: 105
    },
    {
        x: 225,
        y: 110
    },
    {
        x: 230,
        y: 115
    },

    {
        x: 240,
        y: 118
    }

    ];

    regions["lipnolaugh"] = [
    {
        x: 235,
        y: 198
    },
    {
        x: 210,
        y: 190
    },
    {
        x: 200,
        y: 192
    },
    {
        x: 190,
        y: 190
    },
    {
        x: 165,
        y: 198
    },
    {
        x: 180,
        y: 215
    },
    {
        x: 200,
        y: 220
    },
    {
        x: 220,
        y: 215
    }
    ];

    regions["teeth"] = [
    {
        x: 210,
        y: 200
    },
    {
        x: 200,
        y: 202
    },
    {
        x: 190,
        y: 200
    },
    {
        x: 180,
        y: 205
    },
    {
        x: 200,
        y: 210
    },
    {
        x: 220,
        y: 205
    }
    ];

    regions["cheekleft"] = [
    {
        x: 175,
        y: 165
    },
    {
        x: 160,
        y: 150
    },
    {
        x: 140,
        y: 160
    },
    {
        x: 155,
        y: 175
    }
    ];

    regions["cheekright"] = [
    {
        x: 255,
        y: 165
    },
    {
        x: 240,
        y: 150
    },
    {
        x: 220,
        y: 160
    },
    {
        x: 235,
        y: 175
    }
    ];

    // Build anchor point
    for (var region in regions) {
        for (var i = 0; i < regions[region].length; i++) {
            regions[region][i].anchor = buildAnchor(regionLayer, regions[region][i].x, regions[region][i].y, region);
        }
    }

    // Create region
    createCurves(regions);

    /*
     * update the dotted line points before
     * drawing the layer
     */
    regionLayer.beforeDraw(function () {
        curveLayer.draw();
    });

    imageLayer.afterDraw(function () {
        drawLayer.draw();
    });

    stage.add(imageLayer);
    stage.add(drawLayer);
    stage.add(curveLayer);
    stage.add(regionLayer);

    stage.on("mouseover", function () {
        setVisibleControlRegion(true);
    });

    stage.on("mouseout", function () {
        setVisibleControlRegion(false);
    });

    stage.setScale(scale, scale);
}

function loadImage(src) {
    var imageObj = new Image();
    imageObj.onload = function () {
        var imageWidth = imageObj.width;
        var imageHeight = imageObj.height;
        
        if(imageWidth > 350) {
            imageHeight = imageHeight * 350 / imageWidth;
            imageWidth = 350;
        }

        var processImage = new Kinetic.Image({
            x: (stage.getWidth() - imageWidth) / 2,
            y: 0,
            image: imageObj,
            width: imageWidth,
            height: imageHeight,
            id: "image"
        });

        imageLayer.add(processImage);
        imageLayer.draw();

        creamImage = new Kinetic.Image({
            x: 0,
            y: 0,
            width: imageLayer.width,
            height: imageLayer.height,
            id: "creamImage"
        });
        drawLayer.add(creamImage);

        silkImage = new Kinetic.Image({
            x: 0,
            y: 0,
            width: imageLayer.width,
            height: imageLayer.height,
            id: "silkImage"
        });
        drawLayer.add(silkImage);

        cheekImage1 = new Kinetic.Image({
            x: 0,
            y: 0,
            width: imageLayer.width,
            height: imageLayer.height,
            id: "cheekImage1"
        });
        drawLayer.add(cheekImage1);

        cheekImage2 = new Kinetic.Image({
            x: 0,
            y: 0,
            width: imageLayer.width,
            height: imageLayer.height,
            id: "cheekImage2"
        });
        drawLayer.add(cheekImage2);

        lip1Image = new Kinetic.Image({
            x: 0,
            y: 0,
            width: imageLayer.width,
            height: imageLayer.height,
            id: "lipImage1"
        });
        drawLayer.add(lip1Image);

        lip2Image = new Kinetic.Image({
            x: 0,
            y: 0,
            width: imageLayer.width,
            height: imageLayer.height,
            id: "eyeImage2"
        });
        drawLayer.add(lip2Image);

        teethImage = new Kinetic.Image({
            x: 0,
            y: 0,
            width: imageLayer.width,
            height: imageLayer.height,
            id: "teethImage"
        });
        drawLayer.add(teethImage);

        eyeImage1 = new Kinetic.Image({
            x: 0,
            y: 0,
            width: imageLayer.width,
            height: imageLayer.height,
            id: "eyeImage1"
        });
        drawLayer.add(eyeImage1);

        eyeImage2 = new Kinetic.Image({
            x: 0,
            y: 0,
            width: imageLayer.width,
            height: imageLayer.height,
            id: "eyeImage2"
        });
        drawLayer.add(eyeImage2);

        drawLayer.draw();
        regionLayer.draw();
    };
    imageObj.src = src;
}

function setVisibleControlRegion(visible) {
    for (var region in regions) {
        var curves = curveLayer.get('#' + region);
        if (curves.length > 0) {
            curves[0].setOpacity(visible ? 1 : 0);
        }
        var knobs = regionLayer.get('.' + region);
        for (var i = 0; i < knobs.length; i++) {
            knobs[i].setOpacity(visible ? 1 : 0);
        }
    }

    regionLayer.draw();
}

function buildAnchor(layer, x, y, region) {
    var anchor = new Kinetic.Circle({
        x: x,
        y: y,
        radius: 3,
        stroke: "#ffffff",
        fill: "#0094FF",
        strokeWidth: 1,
        draggable: true,
        name: region
    });

    // add hover styling
    anchor.on("mouseover", function () {
        document.body.style.cursor = "pointer";
        this.setFill("#FF006E");
        this.setStrokeWidth(2);
        currentId = this.getName();
    });
    anchor.on("mouseout", function () {
        document.body.style.cursor = "default";
        this.setFill("#0094FF");
        this.setStrokeWidth(1);

        currentId = "";
    });
    anchor.on("dragend", function () {
        // Update curve
        isReleased = true;
        updateCurve(currentId);
        curveLayer.draw();
    });
    anchor.on("dragmove", function () {
        updateCurve(currentId);
        curveLayer.draw();
    });

    layer.add(anchor);
    return anchor;
}

function createCurves(regions) {
    for (var region in regions) {
        if (regions[region].length == 0) {
            continue;
        }

        var splinePoints = regions[region];
        var spline = new Kinetic.Blob({
            points: splinePoints,
            stroke: '#FFFFFF',
            strokeWidth: 1,
            lineCap: 'round',
            fillPatternImage: document.getElementById("transparent-pixel"),
            tension: 0.3,
            draggable: true,
            id: region
        });
        curveLayer.add(spline);
        spline.setZIndex(zIndexes.indexOf(region));

        spline.on("mouseover", function () {
            currentId = this.getId();
        });
        spline.on("mouseout", function () {
            currentId = "";
        });
        spline.on("dragend", function () {
            this.setZIndex(zIndexes.indexOf(this.getId()));
            // Update anchors
            updateAnchors(currentId);
            DrawEyelids();
            regionLayer.draw();
        });
    }

    curveLayer.draw();
}

function updateCurve(id) {
    if (id == null || id == "" || curveLayer.get('#' + id).length == 0) {
        return;
    }

    var curve = curveLayer.get('#' + id)[0];

    var points = new Array();
    for (var i = 0; i < regions[id].length; i++) {
        points.push({
            x: (regions[id][i].anchor.getX() - curve.getX()),
            y: (regions[id][i].anchor.getY() - curve.getY())
        });
    }

    curve.setPoints(points);
    DrawEyelids();
    //makeup();
}


function updateAnchors(id) {
    if (id == null || id == "" || regionLayer.get('.' + id).length == 0 || curveLayer.get('#' + id).length == 0) {
        return;
    }

    var curve = curveLayer.get('#' + id)[0];

    var anchors = regionLayer.get('.' + id);

    var points = curve.getPoints();
    for (var i = 0; i < points.length; i++) {
        anchors[i].setPosition((points[i].x + curve.getX()), (points[i].y + curve.getY()));
    }
}

// set Draggable
function DraggableFalse(id) {
    curveLayer.get('#' + id)[0].setDraggable(false);
}


//Zoom
function Zoom(valuezoom) {
    scale = (valuezoom / 10) + 1;
    stage.setScale(scale, scale);
    stage.draw();
};


function initColor() {
    colors["creamlight"] = 0xE3EAFD;
    colors["creamskin"] = 0xCBEDF9;

    colors["silklight"] = 0xC0D3F3;
    colors["silkskin"] = 0xB3CDE4;
    colors["silkdark"] = 0x9AC3DD;

    colors["cheekColor1"] = 0xF2CFFF;
    colors["cheekColor2"] = 0xC2BCF9;
    colors["cheekColor3"] = 0xB6B7F8;
    colors["cheekColor4"] = 0x99A5F3;

    colors["lip1Color1"] = 0x2A73E9;
    colors["lip1Color2"] = 0x2A73F5;
    colors["lip1Color3"] = 0x2A73FF;

    colors["lip2Color1"] = 0xABABFF;
    colors["lip2Color2"] = 0xAED0FF;
    colors["lip2Color3"] = 0x79B2FF;
    colors["lip2Color4"] = 0xD4B2FF;
    colors["lip2Color5"] = 0xAEA0FF;
}

function getPixelsInRegionCanvas(canvasImg, region, callback) {
    var width = canvasImg.width;
    var height = canvasImg.height;

    var regionCanvas = document.createElement("canvas");
    regionCanvas.width = width;
    regionCanvas.height = height;
    var regionContext = regionCanvas.getContext("2d");
    region.toImage({
        callback: function (result) {
            regionContext.drawImage(result, 0, 0);
            var regionData = regionContext.getImageData(0, 0, width, height).data;

            var regionRect = {
                left: 9999999,
                top: 9999999,
                width: 0,
                height: 0
            };

            for (var i = width * height * 4 - 1; i >= 0; i -= 4) {
                if (regionData[i] > 0) {
                    if (regionRect.left > Math.floor(Math.floor(i / 4) % width)) {
                        regionRect.left = Math.floor(Math.floor(i / 4) % width);
                    }
                    if (regionRect.width < Math.floor(Math.floor(i / 4) % width)) {
                        regionRect.width = Math.floor(Math.floor(i / 4) % width);
                    }

                    if (regionRect.top > Math.floor(Math.floor(i / 4) / width)) {
                        regionRect.top = Math.floor(Math.floor(i / 4) / width);
                    }
                    if (regionRect.height < Math.floor(Math.floor(i / 4) / width)) {
                        regionRect.height = Math.floor(Math.floor(i / 4) / width);
                    }
                }
            }

            regionRect.width -= regionRect.left;
            regionRect.height -= regionRect.top;

            // Get raw pixels of original image
            var dataImage = canvasImg.getContext("2d").getImageData(regionRect.left, regionRect.top, regionRect.width, regionRect.height);

            var i = 3;
            for (var y = regionRect.top; y < regionRect.top + regionRect.height; y++) {
                for (var x = regionRect.left; x < regionRect.left + regionRect.width; x++) {
                    if (regionData[4 * (x + y * width) + 3] == 0) {
                        dataImage.data[i] = 0;
                    }
                    i += 4;
                }
            }

            regionRect.image = dataImage;

            callback(regionRect);
        }
    });
}

function getPixelsInRegion(originalImage, region, callback) {
    // Get canvas from image layer
    var canvasImg = originalImage.getCanvas();
    getPixelsInRegionCanvas(canvasImg, region, callback);
}

function drawFoundation(resultImage, originalImage, region, color, fade, blur, strength) {
    getPixelsInRegion(originalImage, region, function (originalRegion) {
        var filteredRegion = glowFilter(originalRegion.image, color, fade, blur, blur, strength, originalRegion.width, originalRegion.height);
        originalRegion.image = filteredRegion;

        Kinetic.Type._getImage(originalRegion.image, function (imageObj) {
            resultImage.setX(originalRegion.left);
            resultImage.setY(originalRegion.top);
            resultImage.setWidth(originalRegion.width);
            resultImage.setHeight(originalRegion.height);
            resultImage.setImage(imageObj);
            resultImage.getLayer().draw();
        });
    });
}

function drawOriginal(resultImage, originalImage, region) {
    getPixelsInRegion(originalImage, region, function (originalRegion) {
        Kinetic.Type._getImage(originalRegion.image, function (imageObj) {
            resultImage.setX(originalRegion.left);
            resultImage.setY(originalRegion.top);
            resultImage.setWidth(originalRegion.width);
            resultImage.setHeight(originalRegion.height);
            resultImage.setImage(imageObj);
            resultImage.getLayer().draw();
        });
    });
}

function drawSilk(resultImage, originalImage, color, fade, strength) {
    var canvas = document.createElement("canvas");
    canvas.width = originalImage.getWidth();
    canvas.height = originalImage.getHeight();
    var context = canvas.getContext("2d");
    context.drawImage(originalImage.getImage(), 0, 0);

    // Get raw pixels of original image
    var dataImage = context.getImageData(0, 0, canvas.width, canvas.height);

    var filteredRegion = glowFilter(dataImage, color, fade, 5, 5, strength, canvas.width, canvas.height);

    Kinetic.Type._getImage(filteredRegion, function (imageObj) {
        resultImage.setX(originalImage.getX());
        resultImage.setY(originalImage.getY());
        resultImage.setWidth(originalImage.getWidth());
        resultImage.setHeight(originalImage.getHeight());
        resultImage.setImage(imageObj);
        resultImage.getLayer().draw();
    });
}


function makeup() {
    // Temporary show region
    setVisibleControlRegion(true);

    drawLayer.clear();
    var face = curveLayer.get("#face")[0];
    if (foundationType != "") {
        drawFoundation(creamImage, imageLayer, face, colors[foundationType], foundationFade, 5, 50);
    }
    if (silkType != "") {
        drawSilk(silkImage, creamImage, colors[silkType], silkFade, 50);
    }

    var cheek1 = curveLayer.get('#cheekleft')[0];
    var cheek2 = curveLayer.get('#cheekright')[0];
    if (cheekColor != "") {

        if (cheekType == 0) {
            drawFoundation(cheekImage1, imageLayer, cheek1, colors[cheekColor], cheekFade, 10, 1);
            drawFoundation(cheekImage2, imageLayer, cheek2, colors[cheekColor], cheekFade, 10, 1);

        }
        else {
            drawFoundation(cheekImage1, imageLayer, cheek1, colors[cheekColor], cheekFade, 10, 2);
            drawFoundation(cheekImage2, imageLayer, cheek2, colors[cheekColor], cheekFade, 10, 2);
        }
    }

    DrawEyelids();
    var lip = curveLayer.get('#lipnolaugh')[0];
    var teeth = curveLayer.get('#teeth')[0];
    //if (lip1Type != "") {
    //    drawFoundation(lip1Image, imageLayer, lip, colors[lip1Type], lip1Fade, 1, 1);
    //}


    var templip = 19 - lip2Fade;
    if (lip2Type != "") {
        drawFoundation(lip2Image, imageLayer, lip, colors[lip2Type], templip, 1, 1);
    }

    if (lip2Type != "" && openMouth == true) {
        drawOriginal(teethImage, drawLayer, teeth);
    }

    setVisibleControlRegion(false);

    stage.draw();
}


//get Position two point on EYE.
function GetPositionEye(id) {
    var region = regions[id];
    var left = 99999, top = 99999, right = 0, bottom = 0;
    for (var i = 0; i < region.length; i++) {
        if (region[i].anchor.attrs.x < left) {
            left = region[i].anchor.attrs.x;
        }
        if (region[i].anchor.attrs.x > right) {
            right = region[i].anchor.attrs.x;
        }
        if (region[i].anchor.attrs.y < top) {
            top = region[i].anchor.attrs.y;
        }
        if (region[i].anchor.attrs.y > bottom) {
            bottom = region[i].anchor.attrs.y;
        }
    }
    return [{ x: left, y: top }, { x: right, y: bottom }, { width: right - left, height: bottom - top }];
}

//Draw eyelids
function DrawEyelids() {
    if (eyeSrc1 == "" || eyeSrc2 == "") {
        return;
    }

    var EyeLeft = GetPositionEye('eyeleft');
    var EyeRight = GetPositionEye('eyeright');

    var imageObj = new Image();
    var imageObj2 = new Image();
    imageObj.onload = function () {
        var curve = curveLayer.get('#eyeleft')[0];
        eyeImage1.setX(EyeLeft[0].x - 10);
        eyeImage1.setY(EyeLeft[0].y - EyeLeft[2].height / 2);
        eyeImage1.setWidth(EyeLeft[2].width + 10);
        eyeImage1.setHeight(EyeLeft[2].height);
        eyeImage1.setImage(imageObj);
        drawLayer.draw();
    };
    imageObj.src = eyeSrc1;
    imageObj2.onload = function () {
        var curve = curveLayer.get('#eyeright')[0];
        eyeImage2.setX(EyeRight[0].x - 10);
        eyeImage2.setY(EyeRight[0].y - EyeRight[2].height / 2);
        eyeImage2.setWidth(EyeRight[2].width + 10);
        eyeImage2.setHeight(EyeRight[2].height);
        eyeImage2.setImage(imageObj2);
        drawLayer.draw();
    };
    imageObj2.src = eyeSrc2;
}

function updateFoundationType(type) {
    if (type == 'creamlight') {
        $('.choose_product_face .note1 a:eq(0) img').attr("src", "Images/light_click.png");
        $('.choose_product_face .note1 a:eq(1) img').attr("src", "Images/skin.png");
    }
    else {
        $('.choose_product_face .note1 a:eq(0) img').attr("src", "Images/light.png");
        $('.choose_product_face .note1 a:eq(1) img').attr("src", "Images/skin_click.png");
    }

    foundationType = type;
    makeup();
}

function updateSilkType(type) {
    if (type == 'silklight') {
        $('.choose_product_face .note2 a:eq(0) img').attr("src", "Images/light_click.png");
        $('.choose_product_face .note2 a:eq(1) img').attr("src", "Images/skin.png");
        $('.choose_product_face .note2 a:eq(2) img').attr("src", "Images/dark.png");
    }
    else if (type == 'silkskin') {
        $('.choose_product_face .note2 a:eq(0) img').attr("src", "Images/light.png");
        $('.choose_product_face .note2 a:eq(1) img').attr("src", "Images/skin_click.png");
        $('.choose_product_face .note2 a:eq(2) img').attr("src", "Images/dark.png");
    }
    else {
        $('.choose_product_face .note2 a:eq(0) img').attr("src", "Images/light.png");
        $('.choose_product_face .note2 a:eq(1) img').attr("src", "Images/skin.png");
        $('.choose_product_face .note2 a:eq(2) img').attr("src", "Images/dark_click.png");
    }

    silkType = type;
    makeup();
}

function updateCheekColor(color) {
    if (color == 'cheekColor1') {
        $('.choose_product_face .note3 a:eq(0) img').attr("src", "Images/color (1)_click.png");
        $('.choose_product_face .note3 a:eq(1) img').attr("src", "Images/color (2).png");
        $('.choose_product_face .note3 a:eq(2) img').attr("src", "Images/color (3).png");
        $('.choose_product_face .note3 a:eq(3) img').attr("src", "Images/color (4).png");
    }
    else if (color == 'cheekColor2') {
        $('.choose_product_face .note3 a:eq(0) img').attr("src", "Images/color (1).png");
        $('.choose_product_face .note3 a:eq(1) img').attr("src", "Images/color (2)_click.png");
        $('.choose_product_face .note3 a:eq(2) img').attr("src", "Images/color (3).png");
        $('.choose_product_face .note3 a:eq(3) img').attr("src", "Images/color (4).png");
    }
    else if (color == 'cheekColor3') {
        $('.choose_product_face .note3 a:eq(0) img').attr("src", "Images/color (1).png");
        $('.choose_product_face .note3 a:eq(1) img').attr("src", "Images/color (2).png");
        $('.choose_product_face .note3 a:eq(2) img').attr("src", "Images/color (3)_click.png");
        $('.choose_product_face .note3 a:eq(3) img').attr("src", "Images/color (4).png");
    }
    else {
        $('.choose_product_face .note3 a:eq(0) img').attr("src", "Images/color (1).png");
        $('.choose_product_face .note3 a:eq(1) img').attr("src", "Images/color (2).png");
        $('.choose_product_face .note3 a:eq(2) img').attr("src", "Images/color (3).png");
        $('.choose_product_face .note3 a:eq(3) img').attr("src", "Images/color (4)_click.png");
    }

    cheekColor = color;
    makeup();
}

function updateCheekType(type) {
    if (type == 0) {
        $('.choose_product_face .note3 a:eq(4) img').attr("src", "Images/style_1_click.png");
        $('.choose_product_face .note3 a:eq(5) img').attr("src", "Images/style_2.png");
    }
    else {
        $('.choose_product_face .note3 a:eq(4) img').attr("src", "Images/style_1.png");
        $('.choose_product_face .note3 a:eq(5) img').attr("src", "Images/style_2_click.png");
    }

    cheekType = type;
    makeup();
}

function updateLip1Type(type) {
    if (type == "lip1Color1") {
        $('.choose_product_lip .note1 a:eq(0) img').attr("src", "Images/lip (1)_click.png");
        $('.choose_product_lip .note1 a:eq(1) img').attr("src", "Images/lip (2).png");
        $('.choose_product_lip .note1 a:eq(2) img').attr("src", "Images/lip (3).png");
    }
    else if (type == "lip1Color2") {
        $('.choose_product_lip .note1 a:eq(0) img').attr("src", "Images/lip (1).png");
        $('.choose_product_lip .note1 a:eq(1) img').attr("src", "Images/lip (2)_click.png");
        $('.choose_product_lip .note1 a:eq(2) img').attr("src", "Images/lip (3).png");
    }
    else {
        $('.choose_product_lip .note1 a:eq(0) img').attr("src", "Images/lip (1).png");
        $('.choose_product_lip .note1 a:eq(1) img').attr("src", "Images/lip (2).png");
        $('.choose_product_lip .note1 a:eq(2) img').attr("src", "Images/lip (3)_click.png");
    }

    lip1Type = type;
    makeup();
}

function updateLip2Type(type) {
    if (type == "lip2Color1") {
        $('.choose_product_lip .note2 a:eq(0) img').attr("src", "Images/red_click.png");
        $('.choose_product_lip .note2 a:eq(1) img').attr("src", "Images/slow_orange.png");
        $('.choose_product_lip .note2 a:eq(2) img').attr("src", "Images/orange.png");
        $('.choose_product_lip .note2 a:eq(3) img').attr("src", "Images/slow_pink.png");
        $('.choose_product_lip .note2 a:eq(4) img').attr("src", "Images/pink.png");
    }
    else if (type == "lip2Color2") {
        $('.choose_product_lip .note2 a:eq(0) img').attr("src", "Images/red.png");
        $('.choose_product_lip .note2 a:eq(1) img').attr("src", "Images/slow_orange_click.png");
        $('.choose_product_lip .note2 a:eq(2) img').attr("src", "Images/orange.png");
        $('.choose_product_lip .note2 a:eq(3) img').attr("src", "Images/slow_pink.png");
        $('.choose_product_lip .note2 a:eq(4) img').attr("src", "Images/pink.png");
    }
    else if (type == "lip2Color3") {
        $('.choose_product_lip .note2 a:eq(0) img').attr("src", "Images/red.png");
        $('.choose_product_lip .note2 a:eq(1) img').attr("src", "Images/slow_orange.png");
        $('.choose_product_lip .note2 a:eq(2) img').attr("src", "Images/orange_click.png");
        $('.choose_product_lip .note2 a:eq(3) img').attr("src", "Images/slow_pink.png");
        $('.choose_product_lip .note2 a:eq(4) img').attr("src", "Images/pink.png");
    }
    else if (type == "lip2Color4") {
        $('.choose_product_lip .note2 a:eq(0) img').attr("src", "Images/red.png");
        $('.choose_product_lip .note2 a:eq(1) img').attr("src", "Images/slow_orange.png");
        $('.choose_product_lip .note2 a:eq(2) img').attr("src", "Images/orange.png");
        $('.choose_product_lip .note2 a:eq(3) img').attr("src", "Images/slow_pink_click.png");
        $('.choose_product_lip .note2 a:eq(4) img').attr("src", "Images/pink.png");
    }
    else {
        $('.choose_product_lip .note2 a:eq(0) img').attr("src", "Images/red.png");
        $('.choose_product_lip .note2 a:eq(1) img').attr("src", "Images/slow_orange.png");
        $('.choose_product_lip .note2 a:eq(2) img').attr("src", "Images/orange.png");
        $('.choose_product_lip .note2 a:eq(3) img').attr("src", "Images/slow_pink.png");
        $('.choose_product_lip .note2 a:eq(4) img').attr("src", "Images/pink_click.png");
    }

    lip2Type = type;
    makeup();
}


function saveImage() {
    setVisibleControlRegion(false);
    stage.setScale(1, 1);
    stage.toDataURL({
        callback: function (result) {
            $.ajax({
                url: "SaveImage.aspx",
                type: "POST",
                data: "image=" + result,
                success: function (path) {
                    alert(path);
                }
            });
        }
    });
}

//Get Position of element in canvas on page
function GetPositionEyeOnPage(id) {
    var points = GetPositionEye(id);
    var p = $("#imageEditor");
    var posx = points[0].x + p.offset().left;
    var posy = points[0].y + p.offset().top + 180;

    return [{ x: posx, y: posy }];
}
