﻿function glowFilter(bitmap, color, fade, blurX, blurY, strength, bitmapWidth, bitmapHeight) {
    var skinR = color & 0xFF;
    var skinG = (color >> 8) & 0xFF;
    var skinB = (color >> 16) & 0xFF;
    var skinGrey = (skinR + skinG + skinB) / 3;

    var sampleR = 0, sampleG = 0, sampleB = 0, sampleGrey = 0;

    var startIndex = bitmap.data.length / 4 - (4 * blurX * (blurY - 1));
    for (var i = startIndex; i < startIndex + blurX * blurY * 4; i += 4) {
        if (sampleR == 0) {
            sampleR = bitmap.data[i];
            sampleG = bitmap.data[i + 1];
            sampleB = bitmap.data[i + 2];
        }
        else {
            sampleR = (sampleR + bitmap.data[i]) / 2;
            sampleG = (sampleG + bitmap.data[i + 1]) / 2;
            sampleB = (sampleB + bitmap.data[i + 2]) / 2;
        }
    }

    sampleGrey = (sampleR + sampleG + sampleB) / 3;

    var coeffR = 0, coeffG = 0, coeffB = 0, coeffA;
    coeffR = (skinR / skinGrey) / (sampleR / sampleGrey);
    coeffG = (skinG / skinGrey) / (sampleG / sampleGrey);
    coeffB = (skinB / skinGrey) / (sampleB / sampleGrey);

    var coeffFade = 1 + fade / 24;
    coeffR = skinR / 255 * coeffFade;
    coeffG = skinG / skinR * coeffFade;
    coeffB = skinB / skinG * coeffFade;

    var distance = Math.max(bitmapWidth / 2, bitmapHeight / 2);

    var centerX = (bitmapWidth - strength) / 2;
    var centerY = (bitmapHeight - strength) / 2;

    for (var i = 0; i < bitmap.data.length; i += 4) {
        var x = Math.floor(i / 4) % bitmapWidth;
        var y = Math.floor(Math.floor(i / 4) / bitmapWidth);

        bitmap.data[i] = normalize(bitmap.data[i] * coeffR);
        bitmap.data[i + 1] = normalize(bitmap.data[i + 1] * coeffG);
        bitmap.data[i + 2] = normalize(bitmap.data[i + 2] * coeffB);

        if (x >= centerX && x <= centerX + strength && y >= centerY && y <= centerY + strength) {
            bitmap.data[i + 3] = bitmap.data[i + 3];
        }
        else {
            var length = Math.sqrt((x - bitmapWidth / 2) * (x - bitmapWidth / 2) + (y - bitmapHeight / 2) * (y - bitmapHeight / 2));

            if (length > strength) {
                bitmap.data[i + 3] = normalize((distance - length) / (distance - strength) * bitmap.data[i + 3]);
            }
        }
    }

    return bitmap;
}

function normalize(color) {
    if (color < 0) {
        return 0;
    }
    else if (color > 255) {
        return 255;
    }
    else {
        return Math.floor(color);
    }
}