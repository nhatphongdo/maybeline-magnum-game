﻿var preload, soundHanlder;

var manifest = [
    { id: "Flip", src: "Content/background.mp3" },
    { id: "Win", src: "Content/win.mp3" }
];


var texts = ["MAGNUM", "Maybelline", "New York", "Dày Mi", "Collagen", "Ấn tượng", "Mascara", "Trang điểm"];
var colors = ["#3C2654", "#4800FF", "#B200FF", "#FF6A00", "#0094FF", "#7F3300", "#FF006E", "#57007F", "#3F647F", "#3F7F62"];

var numberOfCells = 10;

var stage = -1;
// 0 : start game
// 1 : init game & play
// 2 : in game
// 3 : stop game
// 4 : slow down
// 5 : game is stopped

var slowTimerId;
var maxTextSpeed = 30;
var textSpeed = maxTextSpeed;
var textPause = 600;
var previousText = "           ";
var previousColor;

var timer = 0;
var scrollTime;

$(document).ready(function () {
    createjs.FlashPlugin.BASE_PATH = "Content/";
    createjs.SoundJS.registerPlugins([createjs.FlashPlugin]);

    preload = new createjs.PreloadJS();
    preload.installPlugin(createjs.SoundJS);
    preload.loadManifest(manifest);
    preload.onComplete = function () {
        soundHanlder = createjs.SoundJS.play("Flip", createjs.SoundJS.INTERRUPT_ANY, 0, 0, -1, 1, 0);
        soundHanlder.stop();

        $('body').css("width", window.innerWidth);
        $('body').css("height", window.innerHeight);

        $('.start-button').css("margin-top", window.innerHeight);
        $('.start-button').animate({
            "font-size": 20,
            "margin-top": window.innerHeight - 160
        }, 1000, function () {
            ++stage;
        });

        $('body').on("keydown", function (event) {
            if (event.keyCode == 13) {
                if (stage == 0) {
                    // Start game
                    ++stage;
                    $('#splash').fadeOut();
                    $('#textContainer').fadeIn(500, function () {
                        initProgress();
                        startProgress();

                        ++stage;
                    });
                }
                else if (stage == 2) {
                    // Stop game
                    ++stage;

                    slowTimerId = setInterval("slowDown()", 200);
                }
                else if (stage == 6) {
                    // Reset game
                    window.location.href = "Default.aspx";
                }
            }
        });
    };
});

function initProgress() {
    textSpeed = maxTextSpeed;

    $('#textContainer').html("");
    for (var i = 0; i < numberOfCells; i++) {
        var container = $("<div class='number-counter'><div class='inner knob knob-left'></div><div class='inner divider'></div><div class='inner number'><div id='slot"
            + i + "_l' class='slot'></div><div id='slot" + i + "_r' class='slot'></div></div><div class='inner knob knob-right'></div></div>");
        $('#textContainer').append(container);
    }

    $('#textContainer').append("<div class='guide-text'>Nhấn ENTER để chọn chữ</div>");

    $('.number-counter:eq(0)').css("margin-left", (window.innerWidth - 80 * numberOfCells) / 2);
    $('.number-counter').css("margin-top", window.innerHeight - 140);
}

function generateText() {
    // Set text
    var textIndex = Math.floor(Math.random() * (texts.length * 10)) % texts.length;
    if (stage == 3) {
        ++stage;
        if (gotPrize === true) {
            textIndex = 0;
        }
        else if (textIndex == 0) {
            textIndex = 1;
        }
    }

    var colorIndex = Math.floor(Math.random() * (colors.length - 1)) + 1;
    if (textIndex == 0) {
        colorIndex = 0;
    }

    var text = texts[textIndex];
    for (var i = (numberOfCells - text.length) / 2; i > 0; i--) {
        text = " " + text;
    }
    for (var i = text.length; i < numberOfCells; i++) {
        text = text + " ";
    }
    for (var i = 0; i < numberOfCells; i++) {
        var slot = $('#slot' + i + "_l");
        slot.css("color", colors[colorIndex]);
        slot.text(text[i]);

        $('#slot' + i + "_r").css("color", previousColor);
        $('#slot' + i + "_r").text(previousText[i]);
    }

    previousColor = colors[colorIndex];
    previousText = text;
}

function startProgress() {
    // Play sound
    soundHanlder.play(createjs.SoundJS.INTERRUPT_ANY, 0, 0, -1, 1, 0);

    generateText();

    // Reset position
    for (var i = 0; i < numberOfCells; i++) {
        var slot = $('#slot' + i + "_r");
        slot.css("top", 0);
        $('#slot' + i + "_l").css("top", -parseInt(slot.css("height")));
    }

    // Scroll time
    scrollTime = (Math.floor(Math.random() * 6) + 8) * 250;

    textAnimationIn();
}

function textAnimationIn() {
    timer += 1000 / 30;

    var isFinished = false;
    for (var i = 0; i < numberOfCells; i++) {
        $('#slot' + i + "_r").css("top", parseInt($('#slot' + i + "_r").css("top")) + textSpeed);

        var slot = $('#slot' + i + "_l");
        if (parseInt(slot.css("top")) >= 0) {
            isFinished = true;
            continue;
        }

        slot.css("top", parseInt(slot.css("top")) + textSpeed);
    }

    if (isFinished) {
        // Re-align text
        for (var i = 0; i < numberOfCells; i++) {
            var slot = $('#slot' + i + "_l");
            slot.css("top", 0);
            $('#slot' + i + "_r").css("top", -parseInt(slot.css("height")));
        }

        if (stage >= 4) {
            // Finish game
            ++stage;

            setTimeout("stopGame()", 1000);
        }
        else {
            // Sleep a little bit after showing
            if (timer >= scrollTime) {
                setTimeout("prepareOut();", textPause);
                timer = 0;
                scrollTime = (Math.floor(Math.random() * 6) + 8) * 250;
            }
            else {
                prepareOut();
            }
        }
    }
    else {
        setTimeout("textAnimationIn()", 1000 / 30);
    }
}

function prepareOut() {
    generateText();

    // Reset position
    for (var i = 0; i < numberOfCells; i++) {
        var slot = $('#slot' + i + "_r");
        slot.css("top", 0);
        $('#slot' + i + "_l").css("top", -parseInt(slot.css("height")));
    }

    textAnimationIn();
}

function slowDown() {
    if (textSpeed > 6) {
        textSpeed -= 2;
    }
}

function stopGame() {
    if (stage > 5) {
        return;
    }

    clearInterval(slowTimerId);
    ++stage;

    soundHanlder.stop();

    $('#resultPane').fadeIn(500, function () {
        if (gotPrize) {
            $('#resultText').html("Bạn đã trúng thưởng 1 vé tham dự <br />&ldquo;Đẹp Fashion Show&rdquo; trị giá 1.000.000 đồng");

            createjs.SoundJS.play("Win", createjs.SoundJS.INTERRUPT_ANY, 0, 0, 1, 1, 0);

            //initCanvas();
            //initBalloons();
        }
        else {
            $('#resultText').html("Bạn chưa may mắn lần này rồi.<br />Hãy thử lại lần sau nhé.");
        }
    });
}



var canvas, context, garden;
function initCanvas() {
    canvas = document.getElementById("resultCanvas");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    context = canvas.getContext("2d");

    var flowerCanvas = document.getElementById("flowerCanvas");
    flowerCanvas.width = window.innerWidth;
    flowerCanvas.height = window.innerHeight;
    var flowerContext = flowerCanvas.getContext("2d");
    flowerContext.globalCompositeOperation = "lighter";
    garden = new Garden(flowerContext, flowerCanvas);
    setInterval(function () {
        garden.render();
    }, Garden.options.growSpeed);
}


function animFlower() {
    var x = Math.floor(Math.random() * window.innerWidth);
    var y = Math.floor(Math.random() * window.innerHeight);
    garden.createRandomBloom(x, y);
    setTimeout("animFlower()", 300);
}


// Configure below - change number of images to render
var no = 18;
// Configure speed below
var speed = 20;   // The smaller the number, the faster the movement

var floatr = new Array();
//  Your image location
floatr[0] = "Content/balloon1.png";
floatr[1] = "Content/balloon2.png";
floatr[2] = "Content/balloon3.png";
floatr[3] = "Content/balloon4.png";
floatr[4] = "Content/balloon5.png";
floatr[5] = "Content/balloon6.png";
floatr[6] = "Content/balloon7.png";

var balloons = new Array();


var dx, xp, yp;    // coordinate and position variables
var am, stx, sty;  // amplitude and step variables
var i, doc_width, doc_height;

function initBalloons() {
    doc_width = window.innerWidth;
    doc_height = window.innerHeight * 2;

    balloons = new Array();

    dx = new Array();
    xp = new Array();
    yp = new Array();
    am = new Array();
    stx = new Array();
    sty = new Array();
    j = 0;

    for (i = 0; i < no; ++i) {
        dx[i] = 0; // set coordinate variables
        xp[i] = Math.random() * (doc_width - 50); // set position variables
        yp[i] = Math.random() * doc_height;
        am[i] = Math.random() * 20; // set amplitude variables
        stx[i] = 0.02 + Math.random() / 10; // set step variables
        sty[i] = 0.7 + Math.random(); // set step variables

        var image = new Image();
        image.src = floatr[j];

        balloons.push({
            balloon: image,
            top: 15,
            left: 15
        });

        if (j == (floatr.length - 1)) {
            j = 0;
        } else {
            j += 1;
        }
    }

    animBalloon();
}

function animBalloon() {
    context.clearRect(0, 0, canvas.width, canvas.height);

    for (i = 0; i < no; ++i) {  // iterate for every dot
        yp[i] -= sty[i];
        if (yp[i] < -50) {
            xp[i] = Math.random() * (doc_width - am[i] - 30);
            yp[i] = doc_height;
            stx[i] = 0.02 + Math.random() / 10;
            sty[i] = 0.7 + Math.random();
            doc_width = window.innerWidth;
            doc_height = window.innerHeight;
        }
        dx[i] += stx[i];

        balloons[i].top = yp[i] + pageYOffset;
        balloons[i].left = xp[i] + am[i] * Math.sin(dx[i]);

        context.drawImage(balloons[i].balloon, balloons[i].left, balloons[i].top);
    }

    setTimeout("animBalloon()", speed);
}


function Vector(x, y) {
    this.x = x;
    this.y = y;
};
Vector.prototype = {
    rotate: function (theta) {
        var x = this.x;
        var y = this.y;
        this.x = Math.cos(theta) * x - Math.sin(theta) * y;
        this.y = Math.sin(theta) * x + Math.cos(theta) * y;
        return this;
    },
    mult: function (f) {
        this.x *= f;
        this.y *= f;
        return this;
    },
    clone: function () {
        return new Vector(this.x, this.y);
    },
    length: function () {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    },
    subtract: function (v) {
        this.x -= v.x;
        this.y -= v.y;
        return this;
    },
    set: function (x, y) {
        this.x = x;
        this.y = y;
        return this;
    }
};

function Petal(stretchA, stretchB, startAngle, angle, growFactor, bloom) {
    this.stretchA = stretchA;
    this.stretchB = stretchB;
    this.startAngle = startAngle;
    this.angle = angle;
    this.bloom = bloom;
    this.growFactor = growFactor;
    this.r = 1;
    this.startR = this.r;
    this.isfinished = false;
    //this.tanAngleA = Garden.random(-Garden.degrad(Garden.options.tanAngle), Garden.degrad(Garden.options.tanAngle));
    //this.tanAngleB = Garden.random(-Garden.degrad(Garden.options.tanAngle), Garden.degrad(Garden.options.tanAngle));
}
Petal.prototype = {
    draw: function () {
        var ctx = this.bloom.garden.ctx;
        var v1, v2, v3, v4;
        for (var i = this.startR; i <= this.r; i += this.growFactor) {
            v1 = new Vector(0, i).rotate(Garden.degrad(this.startAngle));
            v2 = v1.clone().rotate(Garden.degrad(this.angle));
            v3 = v1.clone().mult(this.stretchA); //.rotate(this.tanAngleA);
            v4 = v2.clone().mult(this.stretchB); //.rotate(this.tanAngleB);
            ctx.strokeStyle = this.bloom.c;
            ctx.moveTo(v1.x, v1.y);
            ctx.bezierCurveTo(v3.x, v3.y, v4.x, v4.y, v2.x, v2.y);
            ctx.stroke();
        }
    },
    render: function () {
        if (this.r <= this.bloom.r) {
            this.r += this.growFactor; // / 10;
            this.draw();
        } else {
            this.isfinished = true;
        }
    }
}

function Bloom(p, r, c, pc, garden) {
    this.p = p;
    this.r = r;
    this.c = c;
    this.pc = pc;
    this.petals = [];
    this.garden = garden;
    this.init();
    this.garden.addBloom(this);
}
Bloom.prototype = {
    draw: function () {
        var p, isfinished = true;
        this.garden.ctx.save();
        this.garden.ctx.translate(this.p.x, this.p.y);
        for (var i = 0; i < this.petals.length; i++) {
            p = this.petals[i];
            p.render();
            isfinished *= p.isfinished;
        }
        this.garden.ctx.restore();
        if (isfinished == true) {
            this.garden.removeBloom(this);
        }
    },
    init: function () {
        var angle = 360 / this.pc;
        var startAngle = Garden.randomInt(0, 90);
        for (var i = 0; i < this.pc; i++) {
            this.petals.push(new Petal(Garden.random(Garden.options.petalStretch.min, Garden.options.petalStretch.max), Garden.random(Garden.options.petalStretch.min, Garden.options.petalStretch.max), startAngle + i * angle, angle, Garden.random(Garden.options.growFactor.min, Garden.options.growFactor.max), this));
        }
    }
}

function Garden(ctx, element) {
    this.blooms = [];
    this.element = element;
    this.ctx = ctx;
}
Garden.prototype = {
    render: function () {
        this.ctx.clearRect(0, 0, this.element.width, this.element.height);
        for (var i = 0; i < this.blooms.length; i++) {
            this.blooms[i].draw();
        }
    },
    addBloom: function (b) {
        this.blooms.push(b);
    },
    removeBloom: function (b) {
        var bloom;
        for (var i = 0; i < this.blooms.length; i++) {
            bloom = this.blooms[i];
            if (bloom === b) {
                this.blooms.splice(i, 1);
                return this;
            }
        }
    },
    createRandomBloom: function (x, y) {
        this.createBloom(x, y, Garden.randomInt(Garden.options.bloomRadius.min, Garden.options.bloomRadius.max), Garden.randomrgba(Garden.options.color.min, Garden.options.color.max, Garden.options.color.opacity), Garden.randomInt(Garden.options.petalCount.min, Garden.options.petalCount.max));
    },
    createBloom: function (x, y, r, c, pc) {
        new Bloom(new Vector(x, y), r, c, pc, this);
    },
    clear: function () {
        this.blooms = [];
        this.ctx.clearRect(0, 0, this.element.width, this.element.height);
    }
}

Garden.options = {
    petalCount: {
        min: 10,
        max: 15
    },
    petalStretch: {
        min: 0.1,
        max: 5
    },
    growFactor: {
        min: 0.1,
        max: 1
    },
    bloomRadius: {
        min: 20,
        max: 30
    },
    density: 10,
    growSpeed: 1000 / 60,
    color: {
        min: 0,
        max: 255,
        opacity: 0.5
    },
    tanAngle: 90
};
Garden.random = function (min, max) {
    return Math.random() * (max - min) + min;
};
Garden.randomInt = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};
Garden.circle = 2 * Math.PI;
Garden.degrad = function (angle) {
    return Garden.circle / 360 * angle;
};
Garden.raddeg = function (angle) {
    return angle / Garden.circle * 360;
};
Garden.rgba = function (r, g, b, a) {
    return 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')';
};
Garden.randomrgba = function (min, max, a) {
    return Garden.rgba(Math.round(Garden.random(min, max)), Math.round(Garden.random(min, max)), Math.round(Garden.random(min, max)), a);
};
