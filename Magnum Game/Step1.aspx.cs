﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Magnum_Game
{
    public partial class MakeupStep1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (FileUpload.HasFile)
            {
                var image = new System.Web.UI.WebControls.Image();
                UploadImage(FileUpload, image);
                string[] imagePart = image.ImageUrl.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                string[] Name = {};
                foreach (string n in imagePart)
                {
                    if (n.Contains(".jpg"))
                    {
                        Name = n.Split(new[]{".jpg"}, StringSplitOptions.RemoveEmptyEntries);
                    }
                }
                if(Name.Count() > 0)
                {
                    Response.Redirect("Step2.aspx?image=" + Name[0]);
                }
            }

        }

        protected void UploadImage(FileUpload imageUpload, System.Web.UI.WebControls.Image imageBox)
        {
            if (!imageUpload.FileName.ToLower().EndsWith(".jpg") && !imageUpload.FileName.ToLower().EndsWith(".jpeg") && !imageUpload.FileName.ToLower().EndsWith(".png"))
            {
                //ErrorMessage.Text = "Tập tin ảnh phải có định dạng JPEG hoặc PNG";
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Tập tin ảnh phải có định dạng JPEG hoặc PNG');</SCRIPT>");
                return;
            }

            if (imageUpload.PostedFile.ContentLength > 2024768)
            {
                //ErrorMessage.Text = "Tập tin ảnh phải có dung lượng không quá 2 MB";
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Tập tin ảnh phải có dung lượng không quá 2 MB');</SCRIPT>");
                return;
            }

            var fileName = string.Format("Upload/{0}.jpg", DateTime.Now.Ticks);
            try
            {
            var bitmap = Bitmap.FromStream(imageUpload.PostedFile.InputStream);

            var propertyItems = bitmap.PropertyItems;
            foreach (var property in propertyItems)
            {
                if (property.Id == 274)
                {
                    // Orientation
                    switch (property.Value[0])
                    {
                        case 1:
                            // Keep as original
                            break;
                        case 2:
                            // Flip X
                            bitmap.RotateFlip(RotateFlipType.RotateNoneFlipX);
                            break;
                        case 3:
                            // Rotate right 90 degree
                            bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                            break;
                        case 4:
                            // Flip Y
                            bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
                            break;
                        case 5:
                            // Rotate right 90 degree and flip X
                            bitmap.RotateFlip(RotateFlipType.Rotate90FlipX);
                            break;
                        case 6:
                            // Rotate right 90 degree
                            bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                            break;
                        case 7:
                            // Rotate left 90 degree and flip X
                            bitmap.RotateFlip(RotateFlipType.Rotate270FlipX);
                            break;
                        case 8:
                            // Rotate left 90 degree
                            bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                            break;
                    }
                }
            }

            var width = bitmap.Width;
            var height = bitmap.Height;

            if (width > 350)
            {
                height = height * 350 / width;
                width = 350;
            }

            var resizedBitmap = new Bitmap(bitmap, width, height);
            resizedBitmap.Save(Server.MapPath(fileName), System.Drawing.Imaging.ImageFormat.Jpeg);

            imageBox.ImageUrl = fileName;

            }
            catch (Exception exc)
            {
                //ErrorMessage.Text = "Xảy ra lỗi trong quá trình tải ảnh lên. Bạn hãy tải lại trang và thử lại.";
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Tập tin ảnh phải có dung lượng không quá 2 MB');</SCRIPT>");
                return;
            }
        }
    }
}